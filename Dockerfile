FROM postgres:10.4-alpine

ARG APP_NAME=${APP_NAME}
ENV APP_NAME=${APP_NAME}

ARG USER_DB=${USER_DB}
ENV USER_DB=${USER_DB}

ARG HOST_DB=${HOST_DB}
ENV HOST_DB=${HOST_DB}

ARG POSTGRES_DB=${POSTGRES_DB}
ENV POSTGRES_DB=${POSTGRES_DB}

ARG PORT_DB=${PORT_DB}
ENV PORT_DB=${PORT_DB}

ARG PASSWORD_DB=${PASSWORD_DB}
ENV PASSWORD_DB=${PASSWORD_DB}

COPY crontab-definition /crontab-definition
RUN chmod 0644 /crontab-definition
RUN /usr/bin/crontab /crontab-definition

COPY bkp-db.sh /bkp-db.sh
RUN chmod +x /bkp-db.sh

RUN env >> /etc/environment

RUN touch /var/log/cron.log
CMD crond && tail -f /var/log/cron.log